from usefulStuff import *
import os


class EndScreen:

    # It is called when the game is over, it draws the final screen using the functions bellow
    # and prints the result
    def showResult(self, player1, player2, score1, score2):
        result = ["=" * 80]
        result += [" " * 72 + "m: Menu" + " " * 1]
        result += [" " * 72 + "q: Quit" + " " * 1]
        for i in range(3):
            result += ["" * 80]

        result = getResult(result, player1, player2, score1, score2)
        result += [" " * 80]
        result = drawScoreboard(result, player1, player2, score1, score2)

        for i in range(5):
            result += ["" * 80]

        result += ["=" * 80]
        result += ["" * 80]
        for line in result:
            print(line)

        return nextMove()


# Calculate the result comparing the players' scores
# Add to the result the final result of the game, the name of the winner (if exists)
# and the word WINNER. In case of a draw add the word DRAW
def getResult(result, player1, player2, score1, score2):
    if score1 == score2:
        result += ["" * 80]
        result += ["{: ^80s}".format("DRAW!")]
    elif score1 > score2:
        result += ["{: ^80s}".format(player1)]
        result += ["{: ^80s}".format("WINNER!")]
    else:
        result += ["{: ^80s}".format(player2)]
        result += ["{: ^80s}".format("WINNER!")]

    return result


# Take the names of the players and their scores and draw the final screen,
# which has the score board including the names and the scores
def drawScoreboard(result, player1, player2, score1, score2):
    n1 = maxNameLen * 2 + 7
    n2 = (80 - n1) // 2
    result += [" " * n2 + "-" * n1]
    p1 = (maxNameLen - len(player1)) // 2
    p2 = (maxNameLen - len(player2)) // 2
    result += [" " * n2 + "| " + " " * p1 + player1 + " " * (maxNameLen - len(player1) - p1) + " | " + " " * p2 + player2 + " " * (maxNameLen - len(player2) - p2) + " |"]
    result += [" " * n2 + "-" * n1]
    sc1 = (maxNameLen - 1) // 2
    sc2 = (maxNameLen - 1) // 2
    result += [" " * n2 + "| " + " " * sc1 + str(score1) + " " * (maxNameLen - 1 - sc1) + " | " + " " * sc2 + str(score2) + " " * (maxNameLen - 1 - sc1) + " |"]

    result += [" " * n2 + "-" * n1]

    return result


# Concatenate the message that appears in when the game is over
# and check the answer, calling the proper method
def nextMove():
    s = input("Type m / q:")
    print("" * 80)
    while s.lower() != 'm' and s.lower() != 'q':
        s = input("Wrong input! Please, try again. \nType m / q:")
        print("" * 80)

    if s.lower() == 'm':
        nextScreen()
        return True
    elif s.lower() == 'q':
        os._exit(0)
