import socket
import time
import json
import sys
import os
from gamestate import GameState
from game import Game, formatText
from remote import Message
from timers import ExtendedTimer
from endscreen import EndScreen
from tournament import Tournament

class Client:
    def __init__(self):
        self.connectionInfo = ("127.0.0.1", 9999)
        while True:
            try:
                self.requestState()
            except ConnectionRefusedError as e:
                #print(e)
                pass
            time.sleep(5)

    def playGame(self, playernames):
        self.game = JoinGame(playernames, [None, None])
        #self.gamestate = GameState()
        points = None
        while points is None:
            try:
                points = self.requestGamestate()
            except ConnectionRefusedError as e:
                print("Server exited")
                os._exit(0)
            time.sleep(5)

        msg = ""
        if points[0] == 1 and points[1] == 0:
            msg = "The winner is {:s}!".format(self.game.playernames[0])
        elif points[0] == 0 and points[1] == 1:
            msg = "The winner is {:s}!".format(self.game.playernames[1])
        else:
            msg = "Draw!"
        print()
        print("=" * 80 + "\n" * 10)
        print(msg)
        print("\n" * 10 + "=" * 80)


    def requestState(self):
        msg = Message(command=Message.getState)
        result = self.sendMessage(msg)
        if result.data:
            if result.data.get("tournament"):
                t_dict = result.data["tournament"]
                t = Tournament(t_dict["playernames"], [None]*len(t_dict["playernames"]))
                t.__dict__.update(t_dict)
                t.print(interactive=False)
            if result.data.get("gameNames"):
                playernames = result.data["gameNames"]
                self.playGame(playernames)

    def requestGamestate(self):
        msg = Message(command=Message.getGamestate)
        result = self.sendMessage(msg)
        return self.handleResult(result)

    def handleResult(self, resultMessage):
        if resultMessage.data:
            # data is always a gamestate
            self.game.gamestate.__dict__ = resultMessage.data["gamestate"]
            if self.game.gamestate.timeout or self.game.gamestate.exited or self.game.gamestate.isFinished():
                return self.game.gamestate.points
            self.game.gametimer = ExtendedTimer(resultMessage.data["remaining"], self.game.timeout)
            self.game.commandsText = ["Wait for {:s}".format(self.game.playernames[self.game.gamestate.currentPlayer])]
            self.game.commandsText += [""]*2
            self.game.commandsText += ["(wait)"]
            self.game.redraw()
        if resultMessage.command == Message.selectPiece:
            self.game.gametimer.start()
            self.game.refreshTimer.start()
            try:
                selected = self.game.selectPieceInteractive()
            except Exception as e:
                if e.args[0] == "quit":
                    msg = Message(command=Message.quit_)
                    resultMessage = self.sendMessage(msg)
                    return self.handleResult(resultMessage)

            self.game.gametimer.cancel()
            self.game.refreshTimer.cancel()
            if self.game.gamestate.timeout:
                self.game.gamestate.isFinished()
                return self.game.gamestate.points
            if selected is not None:
                msg = Message(command=Message.selectedPiece, data=selected)
            else:
                msg = Message(command=Message.exit_)
            resultMessage = self.sendMessage(msg)
            return self.handleResult(resultMessage)
        elif resultMessage.command == Message.placePiece:
            self.game.gametimer.start()
            self.game.refreshTimer.start()
            try:
                placed = self.game.placePieceInteractive()
            except Exception as e:
                if e.args[0] == "quit":
                    msg = Message(command=Message.quit_)
                    resultMessage = self.sendMessage(msg)
                    return self.handleResult(resultMessage)

            self.game.gametimer.cancel()
            self.game.refreshTimer.cancel()
            if self.game.gamestate.timeout:
                self.game.gamestate.isFinished()
                return self.game.gamestate.points
            if placed is not None:
                msg = Message(command=Message.placedPiece, data=placed)
            else:
                msg = Message(command=Message.exit_)
            resultMessage = self.sendMessage(msg)
            return self.handleResult(resultMessage)
        elif resultMessage.command == Message.quit_:
            os._exit(0)
    
    def sendMessage(self, msg):
        result = None
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.connect(self.connectionInfo)
            sock.send(msg.getBytes())
            result = Message(byteMessage=sock.recv(102451223))
        return result


class JoinGame(Game):
    def quit(self):
        raise Exception("quit")


if __name__ == "__main__":
    Client()