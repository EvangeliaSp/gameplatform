import json
import socketserver
from threading import Thread
import time
import os
import copy
from gamestate import GameState
from ai import Player, EasyAI

class Message():
    getGamestate = "getGamestate"
    selectPiece = "selectPiece"
    selectedPiece = "selectedPiece"
    placePiece = "placePiece"
    placedPiece = "placedPiece"
    quit_ = "quit"
    exit_ = "exit"
    getState = "getState"

    def __init__(self, byteMessage=None, command=None, data=None):
        if byteMessage is None:
            self.command = command
            self.data = data
        else:
            str_msg = byteMessage.decode('utf-8')
            if not str_msg:
                self.command = None
                self.data = None
            else:
                result_dict = json.loads(str_msg)
                self.command = result_dict["command"]
                self.data = result_dict["data"]

    def getBytes(self):
        msg = {"data": self.data, "command": self.command}
        msg_string = json.dumps(msg)
        return msg_string.encode('utf-8')


class Handler(socketserver.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        # self.request is the TCP socket connected to the client
        requestMessage = Message(byteMessage=self.request.recv(1024))
        print("{} wrote:".format(self.client_address[0]))
        print(requestMessage)

        # handle getState request
        if requestMessage.command == Message.getState:
            d = {}
            if self.server.tournament is not None:
                tournamentdict = self.server.tournament.getDict()
                d.update({"tournament": tournamentdict})
            if self.server.game is not None:
                d.update({"gameNames": self.server.game.playernames})
            responseMessage = Message(data=d)
            self.request.sendall(responseMessage.getBytes())
            return

        if self.server.game is not None:
            if requestMessage.command == Message.getGamestate:
                pass # nothing to do
            elif requestMessage.command == Message.selectedPiece:
                self.server.commandResult = requestMessage.data
            elif requestMessage.command == Message.placedPiece:
                self.server.commandResult = requestMessage.data
            elif requestMessage.command == Message.quit_:
                # received a quit message, send a quit message back and exit
                self.request.sendall(Message(command=Message.quit_).getBytes())
                os._exit(0)
            elif requestMessage.command == Message.exit_:
                self.server.commandResult = Message.exit_
            
            # all responses contain the gamestate as data
            d = {"gamestate": self.server.game.gamestate.__dict__,
                "remaining": self.server.game.gametimer.remaining()}
            responseMessage = Message(data=d)
            if self.server.command is not None:
                responseMessage.command = self.server.command

            self.request.sendall(responseMessage.getBytes())
            self.server.command = None
            if self.server.game.gamestate.isFinished():
                self.server.game = None

class MyServer(socketserver.TCPServer):
    def __init__(self, connectionInfo, handler):
        super().__init__(connectionInfo, handler)
        self.game = None
        self.tournament = None
        self.command = None
        self.commandResult = None


class Singleton(type):
    """
    Define an Instance operation that lets clients access its unique
    instance.
    source: sourcemaking.com
    """

    def __init__(cls, name, bases, attrs, **kwargs):
        super().__init__(name, bases, attrs)
        cls._instance = None

    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__call__(*args, **kwargs)
        return cls._instance


class RemotePlayer(Player, metaclass=Singleton):
    def __init__(self):
        self.server = MyServer(("localhost", 9999), Handler)
        self.serverThread = Thread(target=self.server.serve_forever)
        self.serverThread.start()

    def selectPiece(self, gamestate):
        self.server.command = Message.selectPiece
        while self.server.commandResult is None and self.server.game.gamestate.timeout == False:
            time.sleep(1)
        selected = self.server.commandResult
        self.server.commandResult = None
        if selected == Message.exit_:
            gamestate.exited = True
        return selected

    def placePiece(self, gamestate):
        self.server.command = Message.placePiece
        while self.server.commandResult is None and self.server.game.gamestate.timeout == False:
            time.sleep(1)
        placed = self.server.commandResult
        self.server.commandResult = None
        if placed == Message.exit_:
            gamestate.exited = True
        return placed
