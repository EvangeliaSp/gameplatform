from ai import HardAI
from gamestate import GameState

def test1():
    # must place on 12 to win
    g = GameState()
    g.placement = [12, 2, 1, 14, 0, 3, 8, 7, 4, 5, 11, 9, None, 13, 6, None]
    g.remaining = [False, False, False, False, False, False, False, False, False, False, True, False, False, False, False, True]
    g.selected = 10
    return g, 12


def test2():
    # immediate win with place 3
    # artificial setup
    g = GameState()
    g.placement = [0, 1, 2, None, None, None, None, None, None, None, None, None, None, None, None, None]
    g.remaining = [False, False, False, True, False, False, False, False, False, False, False, False, False, False, False, False]
    g.selected = 3
    return g, 3

def test3():
    # must place on 3 so that the other doesn't win
    # artificial setup
    g = GameState()
    g.placement = [0, 1, 2, None, None, None, None, None, None, None, None, None, None, None, None, None]
    g.remaining = [False, False, False, True, False, False, False, False, False, False, False, False, False, False, False, True]
    g.selected = 15
    return g, 3


if __name__ == "__main__":
    h = HardAI()
    for test in [test1, test2, test3]:
        g, goal = test()
        # hard ai must place on goal, otherwise test fails
        h.myID = g.currentPlayer
        result = h.placePiece(g)
        print(result == goal)
