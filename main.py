from menu import mainMenu
from endscreen import EndScreen
from usefulStuff import nextScreen
from remote import RemotePlayer

# main entry point for the application
# shows the menus and starts the game
def main():
    restart = True
    while restart:
        # game or tournament
        gametournament = mainMenu()

        points = gametournament.play()
        for p in gametournament.players:
            if isinstance(p, RemotePlayer):
                p.server.tournament = None

        if points is not None:
            # end of the game
            nextScreen()
            es = EndScreen()
            restart = es.showResult(gametournament.playernames[0], gametournament.playernames[1], points[0], points[1])

# main entry point calling the main() method
if __name__ == '__main__':
    main()
