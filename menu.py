import os
from ai import AIFactory
from remote import RemotePlayer
from tournament import Tournament
from game import Game

# draw the main menu
# returns:
# - aiType = None, 0, 1, 2 (easy,medium, hard)
# - playername(s)
def mainMenu():
    result = ["=" * 80]
    result += [" " * 72 + "q: Quit" + " " * 1]
    result += [" " * 80]

    result += [" " * 20 + "Welcome to board game'SEPM Group D'" + " " * 20]
    result += [" " * 80]
    result += [" " * 20 + "A : Press A to play a single game" + " " * 20]
    result += [" " * 20 + "B : Press B to start a tournament" + " " * 20]
    result += [" " * 20 + "Q : Press Q to quit the game" + " " * 20]
    result += [" " * 80]

    result += ["=" * 80]

    for line in result:
        print(line)

    inp = input("Type A / B / Q :  ")
    if inp.lower() == 'a':
        playernames, player = normalscreen()
        return Game(playernames, player)
    elif inp.lower() == 'b':
        return tournamentscreen()
    elif inp.lower() == 'q':
        os.exit_(0)
    else:
        result += [" " * 80]
        result += [" " * 20 + "Wrong input, please try again! " + " " * 20]
        result += [" " * 80]

        for line in result:
            print(line)
        return mainMenu()


# draw the menu to select the player names
def enterNameScreen(name="Player 1"):
    while True:
        name = input("Enter name of {:s}:".format(name))
        if len(name) <= 15:
            break
        print("The name you entered is too long, please try a shorter name.\n")
    return name


def normalscreen():
    result = [" " * 80]
    result += ["=" * 80]
    result += [" " * 72 + "q: Quit" + " " * 1]
    result += [" " * 20 + "A : Play two player local game" + " " * 20]
    result += [" " * 20 + "B : Play against AI" + " " * 20]
    result += [" " * 20 + "C : Simulate game with 2 AIs" + " " * 20]
    result += [" " * 20 + "D : Play against remote player" + " " * 20]
    result += [" " * 80]
    result += ["=" * 80]

    for line in result:
        print(line)

    inp = input("Type A-D / Q: ")
    if inp.lower() == 'a':
        playernames = [enterNameScreen("Player 1"), enterNameScreen("Player 2")]
        return playernames, [None, None]
    elif inp.lower() == 'b':
        playername = enterNameScreen("Player")
        ainame = "Darth Vader"
        ai = AIFactory.createAI(selectAIDifficulty(ainame))
        return [playername, ainame], [None, ai]
    elif inp.lower() == 'c':
        ainames = ['AI_1','AI_2']
        ai1 = AIFactory.createAI(selectAIDifficulty(ainames[0]))
        ai2 = AIFactory.createAI(selectAIDifficulty(ainames[1]))
        return ainames, [ai1, ai2]
    elif inp.lower() == 'd':
        playernames = [enterNameScreen("Player 1"), enterNameScreen("Player 2 (remote)")]
        remoteplayer = RemotePlayer()
        return playernames, [None, remoteplayer]
    elif inp.lower() == 'q':
        os.exit_(0)
    else:
        result += [" " * 80]
        result += [" " * 20 + "Wrong input, please try again! " + " " * 20]
        result += [" " * 80]

        for line in result:
            print(line)
        return normalscreen()


# draw the menu to select the game type
# returns:
# - aiType = 0, 1, 2 (easy,medium, hard)
# - playername
def selectAIDifficulty(ainame="AI"):
    result = [" " * 80]
    result += ["=" * 80]
    result += [" " * 72 + "q: Quit" + " " * 1]
    result += [" " * 20 + "Select difficulty level of {:s}: ".format(ainame) + " " * 20]
    result += [" " * 20 + "A : easy AI" + " " * 20]
    result += [" " * 20 + "B : medium AI" + " " * 20]
    result += [" " * 20 + "C : hard AI" + " " * 20]
    result += [" " * 80]
    result += ["=" * 80]

    for line in result:
        print(line)

    inp = input("Type A-C / Q: ")
    if inp.lower() in ['a', 'b', 'c']:
        difficulty = ['a', 'b', 'c'].index(inp.lower())
        return difficulty
    elif inp.lower() == 'q':
        os.exit_(0)
    else:
        result += [" " * 80]
        result += [" " * 20 + "Wrong input, please try again! " + " " * 20]
        result += [" " * 80]

        for line in result:
            print(line)
        selectAIDifficulty(ainame=ainame)


# draw the screen that asks the number of players and return this number
def tournamentPlayersNumber():
    result = [" " * 80]
    result += ["=" * 80]
    result += [" " * 72 + "q: Quit" + " " * 1]
    result += [" " * 80]
    result += [" " * 80]
    result += [" " * 10 + "To create a tournament,  you first need to define the number" + " " * 20]
    result += [" " * 10 + "of ALL players (human and AI)." + " " * 20]
    result += [" " * 10 + "(The total number of players must be between 3 and 8)" + " " * 20]
    result += [" " * 80]
    result += [" " * 80]
    result += ["=" * 80]

    for line in result:
        print(line)

    totalnum = input("Input the total players number: ")
    if totalnum.lower() == 'q':
        os.exit_(0)

    while (not totalnum.isdigit()) or (int(totalnum) < 3) or (int(totalnum) > 8):
        result = [" " * 80]
        result += ["=" * 80]
        result += [" " * 72 + "q: Quit" + " " * 1]
        result += [" " * 80]
        result += [" " * 80]
        result += [" " * 10 + "Wrong number of players. Please, try again!" + " " * 20]
        result += [" " * 10 + "(The total number of players must be between 3 and 8)" + " " * 20]
        result += [" " * 80]
        result += [" " * 80]
        result += [" " * 80]
        result += ["=" * 80]

        for line in result:
            print(line)

        totalnum = input("Input the total players number: ")
        if totalnum.lower() == 'q':
            os.exit_(0)

    return int(totalnum)

# draw the screen when we select all the players to be AIs and remote, without choosing at least 1 local player
def localCounterScreen():
    result = [" " * 80]
    result += [" " * 10 + "You need at least 1 local player." + " " * 20]
    result += [" " * 10 + "Please, try again selecting a local player!" + " " * 20]
    result += [" " * 80]
    for line in result:
        print(line)

# draw the screen when we have selected more that one remote player
def remoteCounterScreen():
    result = [" " * 80]
    result += [" " * 10 + "You cannot have more than 1 remote players." + " " * 20]
    result += [" " * 10 + "Please, try again selecting another type for this player!" + " " * 20]
    result += [" " * 80]
    for line in result:
        print(line)


def tournamentscreen():
    totalnum = tournamentPlayersNumber()

    playername = []
    playertype = [None]*totalnum
    localcounter = 0
    aicounter = 0
    remotecounter = 0

    i = 0

    while i < totalnum:
        result = [" " * 80]
        result += ["=" * 80]
        result += [" " * 72 + "q: Quit" + " " * 1]
        result += [" " * 80]
        result += [" " * 80]
        result += [" " * 20 + "Type of Player " + str(i) + ":" + " " * 20]
        result += [" " * 23 + "A : local player" + " " * 20]
        result += [" " * 23 + "B : AI" + " " * 20]
        result += [" " * 23 + "C : remote player" + " " * 20]
        result += [" " * 80]
        result += ["=" * 80]

        for line in result:
            print(line)

        inp = input("Type A / B / C / Q :  ")
        print(inp)
        if inp.lower() == 'a':
            localcounter += 1
            playername += [enterNameScreen("Player " + str(i))]
        elif inp.lower() == 'b':
            aicounter += 1
            if i == totalnum-1 and localcounter == 0:
                aicounter -= 1
                i -= 1
                localCounterScreen()
            elif aicounter == totalnum:
                aicounter -= 1
                i -= 1
                localCounterScreen()
            else:
                playername += ["AI_" + str(aicounter)]
                playertype[i] = AIFactory.createAI(selectAIDifficulty("AI_" + str(i)))
        elif inp.lower() == 'c':
            remotecounter += 1
            if i == totalnum-1 and localcounter == 0:
                remotecounter -= 1
                i -= 1
                localCounterScreen()
            elif remotecounter > 1:
                remotecounter -= 1
                i -= 1
                remoteCounterScreen()
            else:
                playername += [enterNameScreen("Remote 1")]
                playertype[i] = RemotePlayer()
        elif inp.lower() == 'q':
            os.exit_(0)
        else:
            i -= 1
            result += [" " * 80]
            result += [" " * 20 + "Wrong input, please try again! " + " " * 20]
            result += [" " * 80]

            for line in result:
                print(line)

        i += 1

    # create the tournament giving the names of the players
    tournament = Tournament(playername, playertype)

    return tournament


