# Stores a specific state of the game
class GameState():
    # creates the GameState at the beginning of the game
    def __init__(self):
        self.placement = [None]*16
        self.remaining = [True]*16
        self.selected = None
        self.points = [0, 0]
        self.currentPlayer = 0
        self.timeout = False
        self.exited = False

    # place a piece at a specific location
    # return true if successful, otherwise return false
    def placePiece(self, position):
        if self.selected is None:
            return False
        if self.placement[position] is not None:
            return False
        else:
            self.placement[position] = self.selected
            self.remaining[self.selected] = False
            self.selected = None
            return True

    # select a piece of the remaining ones
    # return true if successful, otherwise return false
    def selectPiece(self, position):
        if self.selected is not None:
            return False
        else:
            if not self.remaining[position]:
                return False
            else:
                self.selected = position
                return True

    # check, if 4 pieces are a match
    def isMatch(self, pieces):
        if(any([x is None for x in pieces])):
            return False
        # binary and:
        if (pieces[0] & pieces[1] & pieces[2] & pieces[3]) != 0:
            return True
        elif (pieces[0] | pieces[1] | pieces[2] | pieces[3]) != 15:
            return True
        else:
            return False

    # check, if the game is finished
    # i.e. if there is a match, a timeout, or a draw
    def isFinished(self):
        finished = False

        if self.timeout:
            # will give the points to the correct Player
            self.nextPlayer()
            finished = True

        if self.exited:
            self.nextPlayer()
            finished = True

        # check horizontal:
        for i in range(4):
            l = self.placement[i*4:(i*4)+4]
            if self.isMatch(l):
                finished = True

        # check vertical:
        for i in range(4):
            l = [self.placement[x] for x in [p*4+i for p in range(4)]]
            if self.isMatch(l):
                finished = True

        # check diagonal1:
        l = [self.placement[x] for x in [0, 5, 10, 15]]
        if self.isMatch(l):
            finished = True

        #check diagonal2:
        l = [self.placement[x] for x in [3, 6, 9, 12]]
        if self.isMatch(l):
            finished = True

        if finished:
            self.points[self.currentPlayer] = 1

        # check for draw
        if True not in self.remaining:
            finished = True

        return finished

    # select the next player
    def nextPlayer(self):
        if self.currentPlayer == 0:
            self.currentPlayer = 1
        else:
            self.currentPlayer = 0
