from threading import Timer
import time

# Timer that supports reading the remaining and elapsed time and that can be restarted
class ExtendedTimer:
    started_at = None

    def __init__(self, interval, callback, *args, **kwargs):
        self.interval = interval
        self.callback = callback
        self.args = args
        self.kwargs = kwargs
        self.timer = None

    def start(self):
        self.started_at = time.time()
        self.timer = Timer(self.interval, self.callback, *self.args, **self.kwargs)
        self.timer.start()

    def elapsed(self):
        if self.started_at is None:
            return 0
        return time.time() - self.started_at

    def remaining(self):
        return max(self.interval - self.elapsed(), 0)

    def cancel(self):
        self.started_at = None
        if self.timer is not None:
            self.timer.cancel()

    def restart(self):
        self.cancel()
        self.start()

# Timer, that automatically restarts when the time is over
class RestartingTimer():
    def __init__(self, interval, f, *args, **kwargs):
        self.interval = interval
        self.f = f
        self.args = args
        self.kwargs = kwargs
        self.timer = None

    def callback(self):
        self.f(*self.args, **self.kwargs)
        self.start()

    def start(self):
        self.timer = Timer(self.interval, self.callback)
        self.timer.start()

    def cancel(self):
        if self.timer is not None:
            self.timer.cancel()

    def restart(self):
        self.cancel()
        self.start()
