import sys
import time
import os

from gamestate import GameState
from endscreen import EndScreen
from usefulStuff import *
from timers import ExtendedTimer, RestartingTimer
from ai import AIFactory, AI
from usefulStuff import stringReplace
from remote import RemotePlayer

PIECES = []
STONES = ["(A)"] + ["(*A)"] + ["(B)"] + ["(*B)"] + ["(a)"] + ["(*a)"] + ["(b)"] + ["(*b)"]
STONES += ["|A|"] + ["|*A|"] + ["|B|"] + ["|*B|"] + ["|a|"] + ["|*a|"] + ["|b|"] + ["|*b|"]
for i in range(16):
    PIECES += [" "*6+"\n" + "{: ^6s}\n".format(STONES[i]) + " "*6]

RULE_SUMMARY = """You win this game by placing four boardpieces in a row that
have one equal visual property (letter, bracket, capitalization,
star). Remember to check horizontal, vertical and diagonal! If all
pieces are set and nobody wins, the game ends in a draw."""


# the game class handles the execution and rendering of the game
class Game:
    # the game is initialized with
    # - two playernames
    # - two player types (None=human player or AI class)
    def __init__(self, playernames, players):
        self.gamestate = GameState()
        self.hasSelectedPiece = False
        self.playernames = playernames
        self.gametimer = ExtendedTimer(60.0, self.timeout)
        self.refreshTimer = RestartingTimer(5.0, self.redraw)
        self.commandsText = []
        self.players = players
        for p in self.players:
            if isinstance(p, RemotePlayer):
                p.server.game = self
        self.printGame = True
        if isinstance(players[0], AI) and isinstance(players[1], AI):
            self.printGame = False

    # the play method is called when a game is started
    # it resets the timers and handles the main game loop
    def play(self):
        self.refreshTimer.start()
        while not self.gamestate.isFinished():
            self.gametimer.restart()
            self.nextMove()

        self.refreshTimer.cancel()
        self.gametimer.cancel()
        return self.gamestate.points

    # the nextMove method handles selecting the right next move
    def nextMove(self):
        if self.hasSelectedPiece:
            self.gamestate.nextPlayer()
            self.hasSelectedPiece = False
            self.placePiece()
        else:
            self.selectPiece()

    # the redraw method is called by the refreshTimer and handles drawing of the board and the commands
    def redraw(self):
        if self.printGame:
            print()
            printBoard(round(self.gametimer.remaining()), self.gamestate)
            print("\n".join(self.commandsText), end='')

    # place a piece in the current game
    # this is either done interactively for a local human player or asynchronously for AI or remote player
    def placePiece(self):
        if self.players[self.gamestate.currentPlayer] is None:
            # current Player = human player
            self.placePieceInteractive()
        else:
            # current Player = AI / remote player
            self.placePieceAsync()

    # select a piece in the current game
    # this is either done interactively for a local human player or asynchronously for AI or remote player
    def selectPiece(self):
        if self.players[self.gamestate.currentPlayer] is None:
            # current Player = human player
            self.selectPieceInteractive()
        else:
            # current Player = AI / remote player
            self.selectPieceAsync()

        self.hasSelectedPiece = True

    # let AI / remote player place a piece
    def placePieceAsync(self):
        msg = "Wait for {:s} to place the piece...".format(self.playernames[self.gamestate.currentPlayer])
        self.commandsText = [msg, "", "", "(wait)"]
        self.redraw()

        piece = self.players[self.gamestate.currentPlayer].runPlacePiece(self.gamestate)
        if self.gamestate.timeout:
            input()
            return


    # let AI / remote player select a piece
    def selectPieceAsync(self):
        msg = "Wait for {:s} to select a piece...".format(self.playernames[self.gamestate.currentPlayer])
        self.commandsText = [msg, "", "", "(wait)"]
        self.redraw()

        piece = self.players[self.gamestate.currentPlayer].runSelectPiece(self.gamestate)
        if self.gamestate.timeout:
            input()
            return


    # placePieceInteractive handles one of the two possible moves: placing a piece on the board.
    # this method works interactively with keyboard input from a player
    # it checks for correct inputs and triggers a redraw after placing
    def placePieceInteractive(self):
        command = "{:s}, it's your turn! Type the number of the field you want to place your stone in (position 0-15)! Type q to quit the game or e to give up."
        command = command.format(self.playernames[self.gamestate.currentPlayer])
        shortcommand = "Type q / e / 0-15:"
        self.commandsText = formatText(command) + [""] + [shortcommand]
        self.redraw()

        allowed = ["q", "e"]
        allowed += [str(x) for x in range(16)]

        moveValid = False
        while not moveValid:
            # s = ""
            s = input()
            if self.gamestate.timeout:
                return
            while s.lower() not in allowed:
                self.commandsText[-2] = "This is not possible. Please enter a valid command."
                self.redraw()
                s = input()
                if self.gamestate.timeout:
                    return

            if s.lower() == "q":
                self.quit()

            if s.lower() == "e":
                self.exit()
                return

            if self.gamestate.placePiece(int(s)):
                moveValid = True
                return int(s)
            else:
                self.commandsText[-2] = "This is not possible. Please place the stone in a free position."
                self.redraw()

    # selectPieceInteractive handles one of the two possible moves: selecting one of the remaining pieces
    # this method works interactively with keyboard input from a player
    # it checks for correct inputs and triggers a redraw after selecting
    def selectPieceInteractive(self):
        command = "{:s}, it's your turn! Type the number of the piece you want to select (position 0-15)! Type q to quit the game or e to give up."
        command = command.format(self.playernames[self.gamestate.currentPlayer])
        shortcommand = "Type q / e / 0-15:"
        self.commandsText = formatText(command) + [""] + [shortcommand]
        self.redraw()

        allowed = ["q", "e"]
        allowed += [str(x) for x in range(16)]

        moveValid = False
        while not moveValid:
            # s = ""
            s = input()
            if self.gamestate.timeout:
                return
            while s.lower() not in allowed:
                self.commandsText[-2] = "This is not possible. Please enter a valid command."
                self.redraw()
                s = input()
                if self.gamestate.timeout:
                    return

            if s.lower() == "q":
                self.quit()

            if s.lower() == "e":
                self.exit()
                return

            if self.gamestate.selectPiece(int(s)):
                moveValid = True
                return int(s)
            else:
                self.commandsText[-2] = "This is not possible. Please select an available stone."
                self.redraw()

    # the quit method is called, if the game is quit by pressing q.
    # it stops the timers and exits the application
    def quit(self):
        self.refreshTimer.cancel()
        self.gametimer.cancel()
        os._exit(0)

    # the exit method is called if the plaer presses e and wants to exit.
    def exit(self):
        print("EXIT")
        self.gamestate.exited = True

    # the timeout method is called, if the gametimer runs out.
    # it prints an exit method and triggers a redraw
    def timeout(self):
        if self.printGame:
            self.commandsText[-2] = "The time ran out and {:s} lost! Please press enter to continue.".format(self.playernames[self.gamestate.currentPlayer])
            self.commandsText[-1] = "(Enter)"
            self.redraw()
            self.gamestate.timeout = True

# the printBoard method prints the current board depending on the game state
def printBoard(remaining_time, gamestate):
    result = ["="*80]
    result += [""] * 19
    result += ["="*80]

    # draw left board only
    for i in range(17):
        if i % 4 == 0:
            content = "{: ^39s}".format("*"*29)
        elif i % 2 == 0:
            indices = "*"
            for j in range(i-2, i+2):
                indices += "{: ^6s}".format(str(j)) + "*"
            content = "{: ^39s}".format(indices)
        else:
            content = "{: ^39s}".format(("*"+" "*6)*4+"*")

        result[i+2] += "{: ^39s}".format(content)
    result[1] += " "*39
    result[19] += " "*39

    # draw splitter
    for i in range(19):
        result[i + 1] += "|"


    # draw right area
    for i in range(17):
        content = ""
        if i % 4 == 0:
            content = "{: ^39s}".format(("*"+" "*6)*4+"*")

        result[i+2] += "{: ^39s}".format(content)

    result = drawGameState(result, gamestate)

    result = drawClockSummary(result, remaining_time)

    result += ["="*80]

    print("\n".join(result))


# split text into 80 character lines
def formatText(text):
    split_text = text.split(" ")
    output = []
    while split_text:
        line = ""
        while split_text and len(line + split_text[0]) + 1 < 80:
            line += split_text.pop(0) + " "
        output += [line]
    return output

# returns an array of string lines that can be printed to show the clock and the game rules summary
def drawClockSummary(result, seconds):
    result += [" "* 13]
    result += ["{: ^13s}".format("Remaining:")]
    result += ["{: ^13s}".format("{:02d}:{:02d}".format(seconds // 60, seconds % 60))]
    result += [" "* 13]

    for i in range(4):
        result[-(i+1)] += "| "
        result[-(i+1)] += "{: ^65s}".format(RULE_SUMMARY.split("\n")[-(i+1)])

    return result

# handles drawing the gamestate onto an existing board by replacing strings in an array of string lines
def drawGameState(result, gs):
    for i in range(16):
        if gs.placement[i] is not None:
            result = drawPiece(result, i % 4, i // 4, gs.placement[i], True)

    for i in range(16):
        if gs.remaining[i]:
            result = drawPiece(result, i % 4, i // 4, i, False)

    if gs.selected is not None:
        result = selectPiece(result, gs.selected % 4, gs.selected // 4)

    return result

# draw a single piece at a specific x y position by replacing strings in an array of string lines
def drawPiece(result, x, y, piecenumber, left):
    if x not in range(4):
        return
    if y not in range(4):
        return
    if piecenumber not in range(16):
        return

    x_ = x * 7 + 6
    y_ = y * 4 + 3

    if not left:
        x_ += 40

    result[y_] = stringReplace(result[y_], PIECES[piecenumber].split("\n")[0], x_)
    result[y_+1] = stringReplace(result[y_+1], PIECES[piecenumber].split("\n")[1], x_)
    result[y_+2] = stringReplace(result[y_+2], PIECES[piecenumber].split("\n")[2], x_)

    return result

# draw the selection border by replacing strings in an array of string lines
def selectPiece(result, x, y):
    if x not in range(4):
        return
    if y not in range(4):
        return

    x_ = x * 7 + 6
    y_ = y * 4 + 3

    x_ += 40

    result[y_-1] = stringReplace(result[y_-1], "-"*6, x_)
    result[y_+3] = stringReplace(result[y_+3], "-"*6, x_)

    for i in range(3):
        result[y_+i] = stringReplace(result[y_+i], "|", x_-1)
        result[y_+i] = stringReplace(result[y_+i], "|", x_+6)

    return result
