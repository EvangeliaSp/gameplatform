# maximum length of the player's name
maxNameLen = 15


# print some empty lines to separate the screens
def nextScreen():
    # print("=" * 80)
    for i in range(3):
        print("" * 80)


# replace a part of a string by another by starting at an specific index
def stringReplace(str, new, idx):
    return "".join((str[:idx], new, str[idx+len(new):]))
