from usefulStuff import stringReplace
from game import Game
import random
from remote import RemotePlayer

class Tournament():
    def __init__(self, playernames, players):
        self.playernames = playernames
        self.players = players
        self.winner = [None]
        self.final = [None, None]
        self.semiFinal = [[None, None], [None, None]]
        self.quarterFinal = [[None, None], [None, None], [None, None], [None, None]]
        self.data = [self.quarterFinal, self.semiFinal, [self.final], [self.winner]]
        self.winnerOrder = [None]
        self.finalOrder = [None, None]
        self.semiFinalOrder = [[None, None], [None, None]]
        self.quarterFinalOrder = [[None, None], [None, None], [None, None], [None, None]]
        self.dataOrder = [self.quarterFinalOrder, self.semiFinalOrder, [self.finalOrder], [self.winnerOrder]]
        self.currentRound = 0
        
        if len(self.playernames)>4 and len(self.playernames) <=8:
            emptySlots = len(self.playernames)
            index = 0
            j = 0

            if (len(self.playernames) % 2) != 0:
                self.quarterFinal[0][0] = index
                self.quarterFinalOrder[0][0] = 0
                emptySlots = emptySlots - 1
                index += 1
                j = 1

            for i in range(j, 4):
                if emptySlots > 0:
                    self.quarterFinal[i][0] = index
                    self.quarterFinalOrder[i][0] = 0
                    index += 1
                    self.quarterFinal[i][1] = index
                    self.quarterFinalOrder[i][1] = 1
                    index += 1
                    emptySlots = emptySlots - 2
        elif len(self.playernames) == 4:
            for i in range(4):
                self.semiFinal[i//2][i%2] = i
                self.semiFinalOrder[i//2][i%2] = i%2
            self.quarterFinal = None  # no quarterFinal is played
            self.quarterFinalOrder = None
            self.currentRound = 1  # start at semifinal
        elif len(self.playernames) == 3:
            self.semiFinal[0][0] = 0
            self.semiFinal[1][0] = 1
            self.semiFinal[1][1] = 2
            self.semiFinalOrder[0][0] = 0
            self.semiFinalOrder[1][0] = 0
            self.semiFinalOrder[1][1] = 1
            self.quarterFinal = None  # no quarterFinal is played
            self.quarterFinalOrder = None
            self.currentRound = 1  # start at semifinal
        else:
            raise BaseException("WRONG NUMBER OF PLAYERS!")
        for p in self.players:
            if isinstance(p, RemotePlayer):
                p.server.tournament = self

    def getDict(self):
        d = {}
        d["playernames"] = self.playernames
        d["winner"] = self.winner
        d["final"] = self.final
        d["semifinal"] = self.semiFinal
        d["quarterFinal"] = self.quarterFinal
        return d

    def play(self):
        self.print("Welcome to the tournament!")
        for i in range(self.currentRound, 3):
            matches = self.data[i]
            winners = []
            for j, match in enumerate(matches):
                (winner, order) = self.doMatch(i, j)
                self.data[i+1][j//2][j%2] = winner
                self.dataOrder[i + 1][j // 2][j % 2] = order

            self.print()
        self.printMessageWithEnter("The winner of the tournament is "+str(self.playernames[self.winner[0]]))


    def doMatch(self, round, matchnum):
        msg = "Next up in the tournament:\n"
        match = self.data[round][matchnum]
        order = self.dataOrder[round][matchnum]
        if match[0] is not None and match[1] is not None:
            # TODO check for player order
            if order[0] < order[1]:
                first = match[0]
                second = match[1]
            elif order[1] < order[0]:
                first = match[1]
                second = match[0]
            else:
                first = match[0]
                second = match[1]
            msg += "Match between {:s} and {:s}!".format(self.playernames[first], self.playernames[second])
            self.printMessageWithEnter(msg)
            players = [self.players[first], self.players[second]]
            playernames = [self.playernames[first], self.playernames[second]]
            g = Game(playernames, players)
            result = g.play()
            winner = None
            if result[0] == 1 and result[1] == 0:
                winner = first
            elif result[0] == 0 and result[1] == 1:
                winner = second
            else:
                # draw
                self.printMessageWithEnter("Draw! The winner of this match is determined by a coin flip...")
                winner = random.choice([first, second])
            if winner == match[0]:
                winnerOrder = order[0]
            else:
                winnerOrder = order[1]
            self.printMessageWithEnter("The winner is {:s}!".format(self.playernames[winner]))
            return (winner, winnerOrder)
        elif match[0] is not None:
            msg += "{:s} is automatically qualified to the next round!".format(self.playernames[match[0]])
            self.printMessageWithEnter(msg)
            return (match[0], 0)
        elif match[1] is not None:
            msg += "{:s} is automatically qualified to the next round!".format(self.playernames[match[1]])
            self.printMessageWithEnter(msg)
            return (match[1], 1)
        return (None, None)

    def getMatchOrder(self, round, matchnum):
        if round == 0:
            return self.data[round][matchnum]


    def getFormattedName(self, i):
        if i is not None:
            return "{: ^15s}".format(self.playernames[i])
        else:
            return ""

    def printMessageWithEnter(self, message):
        print("=" * 80 + "\n" * 10)
        print(message)
        print("\n" * 10 + "=" * 80)
        print()
        input("Type Enter to continue")

    def print(self, message="", interactive=True):
        lines = [" "*80]*22
        if len(self.playernames) > 4:
            for i, games in enumerate(self.quarterFinal):
                lines[i*6+1] = stringReplace(lines[i*6+1], self.getFormattedName(games[0]), 0)
                lines[i*6+3] = stringReplace(lines[i*6+3], self.getFormattedName(games[1]), 0)
                # brackets:
                lines[i*6+1] = stringReplace(lines[i*6+1], "------------|", 15)
                lines[i*6+3] = stringReplace(lines[i*6+3], "------------|", 15)

        for i, games in enumerate(self.semiFinal):
            lines[i*12+2] = stringReplace(lines[i*12+2], self.getFormattedName(games[0]), 20)
            lines[i*12+8] = stringReplace(lines[i*12+8], self.getFormattedName(games[1]), 20)
            # brackets:
            lines[i*12+2] = stringReplace(lines[i*12+2], "------------|", 35)
            lines[i*12+3] = stringReplace(lines[i*12+3], "|", 47)
            lines[i*12+4] = stringReplace(lines[i*12+4], "|", 47)
            lines[i*12+6] = stringReplace(lines[i*12+6], "|", 47)
            lines[i*12+7] = stringReplace(lines[i*12+7], "|", 47)
            lines[i*12+8] = stringReplace(lines[i*12+8], "------------|", 35)

        #final
        lines[5] = stringReplace(lines[5], self.getFormattedName(self.final[0]), 40)
        lines[17] = stringReplace(lines[17], self.getFormattedName(self.final[1]), 40)
        #brackets
        lines[5] = stringReplace(lines[5], "------------|", 55)
        for i in list(range(6, 11)) + list(range(12, 17)):
            lines[i] = stringReplace(lines[i], "|", 67)
        lines[17] = stringReplace(lines[17], "------------|", 55)        

        #winner
        lines[11] = stringReplace(lines[11], self.getFormattedName(self.winner[0]), 60)

        lines = ["", message, ""] + lines

        print("\n".join(lines))
        if interactive:
            input("Press Enter to continue")
