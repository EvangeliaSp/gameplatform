import abc
import copy
import time
import random



class AIFactory:

    def createAI(aiType):
        if aiType == 0:
            return EasyAI()
        elif aiType == 1:
            return MediumAI()
        elif aiType == 2:
            return HardAI()
        else:
            pass


class Player():
    def runPlacePiece(self, gamestate):
        result = self.placePiece(gamestate)
        if not (gamestate.timeout or gamestate.exited):
            valid = gamestate.placePiece(result)
            if not valid:
                raise BaseException("The AI or remote player did an invalid move! Something is wrong.")

    def runSelectPiece(self, gamestate):
        result = self.selectPiece(gamestate)
        if not (gamestate.timeout or gamestate.exited):
            valid = gamestate.selectPiece(result)
            if not valid:
                raise BaseException("The AI or remote player did an invalid move! Something is wrong.")

    @abc.abstractmethod
    def selectPiece(self, gamestate):
        pass

    @abc.abstractmethod
    def placePiece(self, gamestate):
        pass


class AI(Player):
    pass

class EasyAI(AI):
    def selectPiece(self, gamestate):
        idx = random.randint(0, 15)
        while not gamestate.remaining[idx]:
            idx = random.randint(0, 15)
        return idx

    def placePiece(self, gamestate):
        idx = random.randint(0, 15)
        while gamestate.placement[idx] is not None:
            idx = random.randint(0, 15)
        return idx


class MediumAI(AI):
    def __init__(self):
        self.selectLater = None

    def selectPiece(self, gamestate):
        if self.selectLater:
            return self.selectLater
        else:
            return EasyAI().selectPiece(gamestate)

    def placePiece(self, gamestate):
        self.selectLater = None
        if random.choice([True, False]):
            h = HardAI()
            result = h.placePiece(gamestate)
            self.selectLater = h.selectLater
            return result
        else:
            return EasyAI().placePiece(gamestate)


class HardAI(AI):

    def placePiece(self, gamestate):
        self.selectLater = None
        # if 0 or 1 pieces are set, choose random
        freePlacements = len([j for j in range(16) if gamestate.placement[j] is None])
        if freePlacements > 14:
            return EasyAI().placePiece(gamestate)

        self.myID = gamestate.currentPlayer

        if freePlacements > 10:
            self.maxDepth = 2
        elif freePlacements == 9 or freePlacements == 10:
            self.maxDepth = 3
        elif freePlacements < 8:
            self.maxDepth = 1000
        else:
            self.maxDepth = 2

        move, _ = self.minimax(gamestate, 0)
        self.selectLater = move[1]
        return move[0]

    def selectPiece(self, gamestate):
        # at the beginning we just choose random
        if len([j for j in range(16) if gamestate.placement[j] is None]) == 16:
            return EasyAI().selectPiece(gamestate)
        if self.selectLater is None:
            return EasyAI().selectPiece(gamestate)
        return self.selectLater

    def minimax(self, gamestate, depth):
        if gamestate.isFinished():
            return None, self.score(gamestate)

        if depth == self.maxDepth:
            return None, 0

        scores = [] # an array of scores

        moves = [(i,j) for i in range(16) if gamestate.placement[i] is None for j in range(16) if gamestate.remaining[j] and j != gamestate.selected]
        if len([j for j in range(16) if gamestate.remaining[j] and j != gamestate.selected]) == 0:
            # the very end
            moves = [(i, None) for i in range(16) if gamestate.placement[i] is None]

        for move in moves:
            newgamestate = copy.deepcopy(gamestate)
            result = newgamestate.placePiece(move[0])
            if not result:
                raise Exception("WRONG PLACEMENT")
            if move[1] is not None:
                # at the very end, no piece can be selected anymore
                result = newgamestate.selectPiece(move[1])
                if not result:
                    raise Exception("WRONG SELECTION")
            newgamestate.nextPlayer()
            _, s = self.minimax(newgamestate, depth+1)
            scores.append(s)
            # alpha-beta pruning: early quit
            if gamestate.currentPlayer == self.myID:
                # max step
                if s == 10:
                    return move, 10
            else:
                # min step
                if s == -10:
                    return move, -10


        if gamestate.currentPlayer == self.myID:
            # max step
            m = max(scores)
            indices = [i for i, x in enumerate(scores) if x == m]
            idx = random.choice(indices)
            return moves[idx], m
        else:
            # min step
            m = min(scores)
            indices = [i for i, x in enumerate(scores) if x == m]
            idx = random.choice(indices)
            return moves[idx], m

    def score(self, gamestate):

        # the wrong player got the points so we need to switch that
        gamestate.points.reverse()

        curr = gamestate.currentPlayer
        if gamestate.points[self.myID] == 1 and gamestate.points[1-self.myID] == 0:
            return 10
        elif gamestate.points[self.myID] == 0 and gamestate.points[1-self.myID] == 1:
            return -10
        elif gamestate.points[self.myID] == gamestate.points[1-self.myID]:
            return 0
