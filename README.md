# GamePlatform

The UU-GAME game platform (game interface) for the Tic-Tac-Toe game. It must allow human players to play a local UU-GAME. That is, it must show the game interface (e.g., the board, the stones already placed on it, ...) and control the game state (e.g